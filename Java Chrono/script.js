

var seconde = 0;
var milli = 0;
var minute = 0;

var buttonstart = document.getElementById("start");
buttonstart.addEventListener("click",function (){startchrono()}, false);
var buttonpause = document.getElementById("pause");
buttonpause.addEventListener("click",function (){pausechrono()}, false);
var buttonarret = document.getElementById("arreter");
buttonarret.addEventListener("click",function (){arreterchrono()}, false);


function startchrono(){
    valeurchrono = setInterval(chrono,10);
    document.getElementById("start").disabled=true;
}

function chrono(){
    milli++;
    console.log(milli);
    document.getElementById("minute").innerText=minute + " min";
    document.getElementById("seconde").innerText=seconde + " s";
    document.getElementById("milli").innerText=milli + " milli";

    if (milli==100)
    {
        seconde++;
        milli=0;
    }

    if (seconde==60){
        minute++;
        seconde=0;
    }

}

function pausechrono(){
    clearInterval(valeurchrono);
    document.getElementById("start").disabled=false;
}

function arreterchrono(){
    clearInterval(valeurchrono);
    milli = 0;
    seconde = 0;
    minute = 0;
    document.getElementById("minute").innerText = 0 + " min";
    document.getElementById("seconde").innerText = 0 + " seconde";
    document.getElementById("milli").innerText = 0 + " milli";
    document.getElementById("start").disabled=false;
}

